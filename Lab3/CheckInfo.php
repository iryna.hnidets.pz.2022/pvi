<?php
try {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        throw new Exception('Помилка post-запиту');
    }

    $input_data = json_decode(file_get_contents("php://input"), true);

    if (empty($input_data['group']) || empty($input_data['firstname']) || empty($input_data['name']) || empty($input_data['gender']) || empty($input_data['birthday'])) {
        throw new Exception('Щось не записано???');
    }

    if (!preg_match('/^[A-ZА-Я][a-zа-я\s\'-]*$/', $input_data['firstname']) || !preg_match('/^[A-ZА-Я][a-zа-я\s\'-]*$/', $input_data['name'])) {
        throw new Exception('Ім\'я чи прізвище не пройшли перевірку');
    }

    if (!strtotime($input_data['birthday']) || (strtotime($input_data['birthday']) > time())) {
        throw new Exception('Дата народження некоректна');
    }

    $response = array(
        'message' => 'Дані успішно оброблені і збережені',
        'ok' => true,
        'data' => $input_data
    );

    header('Content-Type: application/json');
    echo json_encode($response);
} catch (Exception $e) {
    header("HTTP/1.1 500 Internal Server Error");
    $response = array(
        'message' => $e->getMessage(),
        'ok' => false
    );
    echo json_encode($response);
}
?>
