const CACHE_NAME = 'my-pwa-cache'; 
const urlsToCache = [
 '/', 
 '/index.html',
    '/js/pri.js',
    '/css/style.css',
    'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.0/font/bootstrap-icons.css',
]; 
self.addEventListener('install', function(event)  { 
event.waitUntil( caches.open(CACHE_NAME) .then(function(cache)
 { console.log('Opened cache'); return cache.addAll(urlsToCache); }) ); });