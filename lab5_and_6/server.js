const fs = require('fs');
const express = require('express')
const app = express();
const path = require('path');
const moment = require('moment');
const mongoose = require('mongoose');
mongoose.set('strictQuery', false);

const http = require('http').Server(app);
const io = require('socket.io')(http);

const PORT = process.env.PORT || 3000; 


const bodyParser = require('body-parser');
const { getEnabledCategories } = require('trace_events');
const { partial } = require('lodash');
app.use(bodyParser.json());

app.use(express.static(__dirname));
app.get('/', async (req, res) =>{
  try {
    res.sendFile('index.html', {root:'/'})
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
})

const studentDataSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId, // Specify ObjectId type for _id
  group: String, // Group name
  fname: String, // First name
  lname: String, // Last name
  gender: String, // Gender (Male, Female, etc.)
  birthday: String, // Date of birth (in ISO format YYYY-MM-DD)
  password: String // Password (hashed and secured)
});
const Student = mongoose.model('students', studentDataSchema);

const chatSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  chatname: String,
  participants: [String],
  messages: [{
    sender: String,
    text: String,
  }]
});

const Chat = mongoose.model('chats', chatSchema);

let chats
let students
async function loadData(){
  chats = await Chat.find();
  students = await Student.find();
}
loadData();

let authorizedUsers = []; // масив об'єктів {_id, socketId} тих користувачів, які під'єдналися і авторизувалися

//a new connection
io.on('connection', socket =>{
  console.log('a new user connected!  ' + socket.id);
  

  socket.on('disconnect', ()=>{
    //коли від'єднюється, видаляємо
    let index = authorizedUsers.findIndex(obj => obj.socketId === socket.id);
    let deletedUser = {};
    if(index !== -1){
      deletedUser = authorizedUsers[index];
      authorizedUsers.splice(index, 1);
    }

    console.log('the user ' + socket.id + ' disconnected!')
    for(let user of authorizedUsers){
      io.to(user.socketId).emit('now-offline', deletedUser)
    }
  })

  socket.on('userAuth', data => {
    authorizedUsers.push(data);
    //при авторизації нового користувача у всіх користувачів оновлюється інформація про онлайн на таблиці
    for(let useri of authorizedUsers){
      for(let userj of authorizedUsers){
        io.to(useri.socketId).emit('now-online', userj)
      }
    }
  })

  socket.on('sendMessage', data=> {
    /*
    sender: activeUser._id,
    text: document.getElementById('enter-message').value,
    chat: activeChat.key
    */
    let chat = findElementById(chats, data.chat)
    if(chat === null) console.error('didnt find the needed chat!');

    for(let participant of chat.participants){
      //participant - айді(_id) користувача
      if(participant === data.sender) continue; // якщо айді учасника === айді надсилача то пропустити
      
      let user = findElementById(authorizedUsers, participant);
      if(user !== null){
        socket.to(user.socketId).emit('receiveMessage', data);
      }
    }

    Chat.findById(data.chat)
      .then((chatDoc) => {
        const newMessage = {
          sender: data.sender,
          text: data.text
        };
        chatDoc.messages.push(newMessage);
        return chatDoc.save();
      })
      .then((savedChatDoc) => {
        console.log('Message added successfully!');
      })
      .catch((error) => {
        console.error('Error adding message to mongodb:', error);
      });
  })

  socket.on('addNewParticipant', data =>{
    /* data: chat: _id, newparticipant: _id*/
    //додаємо нового учасника на базу даних

    Chat.findById(data.chat)
      .then((chatDoc) => {
        chatDoc.participants.push(data.newparticipant);
        let newStudentInChat = findElementById(students, data.newparticipant)
        let updatemsg = {
          sender: "-",
          text: newStudentInChat.fname + ' ' + newStudentInChat.lname + ' has been added to the chat'
        }
        chatDoc.messages.push(updatemsg);
        return chatDoc.save();
      })
      .then(() => {
        socket.broadcast.emit('updateChatData', data.chat);
        socket.emit('updateChatData', data.chat);

        loadData();
      })
    .catch((error) => { console.log('error while adding new participant:' + error)})
  })

  socket.on('removeParticipant', data =>{
    //chat: _id; participant: _id
    Chat.findById(data.chat)
    .then((chatDoc) => {
      const indexToRemove = chatDoc.participants.indexOf(data.participant);
      if (indexToRemove !== -1) chatDoc.participants.splice(indexToRemove, 1); 
      else console.log('Учасник з даним _id не знайдений у списку');

      let removedStudentInChat = findElementById(students, data.participant)
      let updatemsg = {
        sender: "-",
        text: removedStudentInChat.fname + ' ' + removedStudentInChat.lname + ' has been removed from the chat'
      }
      chatDoc.messages.push(updatemsg);
      return chatDoc.save();
    })
    .then(() => {
      socket.broadcast.emit('updateChatData', data.chat);
      socket.emit('updateChatData', data.chat);

      loadData();
    })
  .catch((error) => { console.log('error while removing participant:' + error)})
  })

  socket.on('createNewChat', data=>{
    let newchat = new Chat({
      _id: new mongoose.Types.ObjectId(),
      chatname: data.chatname,
      participants: data.participants,
      messages: data.messages
    });

    newchat.save()
    .then(result => {
        console.log('chat created successfully.');

        let chat = {
          _id: newchat._id,
          chatname: newchat.chatname,
          participants: newchat.participants,
          messages: newchat.messages
        }

        for(let p of chat.participants){
          let check = findElementById(authorizedUsers, p);
          if(check !== null){
            socket.to(check.socketId).emit('newChat', chat);
          }
        }
        socket.emit('newChat', chat)
  
        loadData();
    })
    .catch(error => {
        console.error('Error creating a new chat:', error);
    });
  })

})

http.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});

mongoose.connect('mongodb+srv://irynahnidets2022:09.03.2005@cluster0.ijimw1w.mongodb.net/');
app.get('/student-data', async (req, res) => {
  try {
    const allStudents = await Student.find();
    res.status(200).send(JSON.stringify(allStudents));
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error: didn\'t get data about students from the database');
  }
});

app.get('/chat-data', async (req, res) =>{
  try {
    const allChats = await Chat.find();
    res.status(200).send(JSON.stringify(allChats))
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error: didn\'t get data about chats from the database');
  }
})

app.post('/check-student', async (req, res)=>{
  //todo: add / edit
  try {
    let data = req.body;
    let errors = {};
    if(!data.group || data.group == '?') errors.group = 'Group is required';
    let check = isValidString(data.fname);
    if (check != 'success') errors.fname = check;
    check = isValidString(data.lname);
    if(check != 'success') errors.lname = check;
    if(!data.gender || data.gender == '?') errors.gender = 'Gender is required';
    
    const birthday = moment(data.birthday);
    if (!birthday.isValid()) {
      errors.birthday = 'Invalid date format (YYYY-MM-DD)';
    } else if (birthday.isBefore('1950-01-01') || birthday.isAfter('2010-12-31')) {
      errors.birthday = 'Birthday must be between 1950-01-01 and 2010-12-31';
    }
    if(data.password.length === 0) errors.password = 'Please, enter your password!';
    
    if(Object.keys(errors).length > 0){
        res.statusCode = 500;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({success: false, errors: errors}));
    } else {
      let studentObj;
      if (data.todo === 'add'){
        studentObj = new Student({
          _id: new mongoose.Types.ObjectId(),
          group: data.group,
          fname: data.fname, 
          lname: data.lname, 
          gender: data.gender, 
          birthday: data.birthday, 
          password: data.password 
        });

        studentObj.save()
        .then(result => {
            console.log('Student added successfully.');
        })
        .catch(error => {
            console.error('Error adding student:', error);
        });

      } else if (data.todo === 'edit'){
        studentObj = new Student({
          _id: data._id,
          group: data.group,
          fname: data.fname, 
          lname: data.lname, 
          gender: data.gender, 
          birthday: data.birthday, 
          password: data.password
        });

        // Оновлення запису за його _id
        Student.findByIdAndUpdate(data._id, studentObj, { new: true })
        .then(updatedStudent => {
            if(updatedStudent) console.log('Student data has been updated.')
            else console.log('The student wasn\'t found by this _id: ' + data._id);
        })
        .catch(error => {
            console.error('Помилка під час оновлення запису:', error);
        });
      }

      loadData();

      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify({success: true, data: studentObj}));
    }
  } catch (error) {
    console.log('an error happened! ' + error)
  }
})

app.post('/delete-student', async (req, res) =>{
  //todo: delete
  
  let dataObj = req.body;
  Student.findByIdAndDelete(dataObj._id)
    .then(deletedStudent => {
        if (!deletedStudent) {
          console.log('The student wasn\'t deleted by their _id: ' + dataObj._id);
        } 
    })
    .catch(error => {
        console.error('Помилка під час видалення студента:', error);
    });

    loadData();
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({success: true, data: dataObj}));
})

function isValidString(str) {
    if (typeof str !== 'string' || str.length === 0) {
        return "Please, fill all the fields.";
    }

    if (!/^[a-zA-Z-]+$/.test(str)) {
        return "The fields should only contain letters and dash symbols.";
    }

    if (!/^[A-Z]/.test(str[0])) {
        return "The first letter in the fields should be capital.";
    }

    for (let i = 1; i < str.length; i++) {
        if (/^[A-Z]/.test(str[i])) {
            if (str[i - 1] === '-') continue;
            else {
                return "Only the first letter and the letter after dash(-) should be capital";
            }
        }
    }

    const dashIndex = str.indexOf('-');
    if (dashIndex !== -1 && !/^[A-Z]/.test(str[dashIndex + 1])) {
        return "The first letter after dash(-) should be capital";
    }

    if (str[str.length - 1] === '-') {
        return "You can't put a dash(-) in the end of your fields";
    }

    return "success";
}

function findElementById(arr, _id) {
  for (const element of arr) {
    if (element._id.toString() === _id.toString()) {
      return element;
    }
  }
  return null;
}

// Функція для видалення елемента за ключем з масиву
function removeItemByKey(arr, key) {
  for (var i = 0; i < arr.length; i++) {
      if (arr[i].key === key) {
          arr.splice(i, 1);
          return;
      }
  }
}

// Функція для отримання значення за ключем з масиву
function getItemByKey(arr, key) {
  for (var i = 0; i < arr.length; i++) {
      if (arr[i].key === key) {
          return arr[i].value;
      }
  }
  return null; // Повертаємо null, якщо ключ не знайдено
}

function addOrUpdateObjectByKey(arr, key, value) {
  // Перевіряємо, чи є у масиві об'єкт з ключем, який ми шукаємо
  var found = false;
  for (var i = 0; i < arr.length; i++) {
      if (arr[i].key === key) {
          // Якщо знайдено об'єкт з таким ключем, замінюємо його значення
          arr[i].value = value;
          found = true;
          break;
      }
  }
  
  // Якщо об'єкт з таким ключем не був знайдений, додаємо новий об'єкт до масиву
  if (!found) {
      arr.push({ key: key, value: value });
  }
}

function isStringInArray(array, searchString) {
  // Перевіряємо кожен елемент масиву
  for (let i = 0; i < array.length; i++) {
      // Якщо елемент масиву рівний шуканій стрічці, повертаємо true
      if (array[i] === searchString) {
          return true;
      }
  }
  // Якщо не знайдено співпадінь, повертаємо false
  return false;
}