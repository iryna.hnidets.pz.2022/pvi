console.log("hello world");

document.getElementById('add-student-btn').onclick = function () {
    document.getElementById('айді-вікна').style.display = "block";
    
};

document.getElementById('close-window-btn').onclick = function () {
    document.getElementById('айді-вікна').style.display = "none";
};


document.getElementById('close-window-btn').onclick = function () {
    let group = document.getElementById('editGroupSelect').value;
    let firstname = document.getElementById('finame').value;
    let name = document.getElementById('lname').value;
    let gender = document.getElementById('editGenderSelect').value;
    let birthday = document.getElementById('editBirthDate').value;
    // let status = document.getElementById('sname').value;
    if (!validateName(firstname)) {
        alert(" Прізвище має починатися з великої літери!");
        return;
    }

    if (!validateName(name)) {
        alert("Ім'я має починатися з великої літери!");
        return;
    }

    if (!validateBirthday(birthday)) {
        alert("Некоректна дата народження! Вік має бути не більше 100 років.");
        return;
    }

    let formData = {
        group: group,
        firstname: firstname,
        name: name,
        gender: gender,
        birthday: birthday
    };

    console.log(formData);
    let rows = document.querySelectorAll('table ');
    let lastStudentRowIndex = -1;
    for (let i = 1; i < rows.length; i++) {
        let cells = rows[i].getElementsByTagName('td');
        if (cells.length > 0) {
            lastStudentRowIndex = i;
        } else {
            break;
        }
    }
    let table = document.querySelector('table');
    let newRow;
    if (lastStudentRowIndex !== -1) {
        newRow = table.insertRow(lastStudentRowIndex + 1);
    } else {
        newRow = table.insertRow(-1);
    }

    let checkboxCell = newRow.insertCell(0);
    checkboxCell.innerHTML = '<input type="checkbox" class="select-all"  style="transform: scale(1.2);">';

    let groupCell = newRow.insertCell(1);
    groupCell.textContent = group;

    let firsnameCell = newRow.insertCell(2);
    firsnameCell.textContent = firstname;

    let nameCell = newRow.insertCell(3);
    nameCell.textContent = name;

    let genderCell = newRow.insertCell(4);
    genderCell.textContent = gender;

    let birthdayCell = newRow.insertCell(5);
    birthdayCell.textContent = birthday;

    let statusCell = newRow.insertCell(6);
    statusCell.innerHTML = '<div class="status-indicator active"></div>';

    let optionsCell = newRow.insertCell(7);
    optionsCell.innerHTML = '<div class="button-container"><button style="border: none; background-color: #fff;font-size: 23px;"><i class="bi bi-pencil-square edit-icon"></i></button><button style="border: none; background-color: #fff;font-size: 23px; "><i class="bi bi-x-square"></i></button></div>';
    createDeleteIcons();
    document.getElementById('айді-вікна').style.display = "none";
    addSelectAllCheckboxEventListeners();
    addEditIconEventListeners();
    getStudentData();
    let editIcon = optionsCell.querySelector('.edit-icon');
    editIcon.addEventListener('click', function () {
        setEditWindowValues(group, firstname, name, gender, birthday);
        document.getElementById('айді-вікна').style.display = "block";
    });
}
function validateName(name) {
    if (!name.trim()) {
        return false;
    }

    if (!/^[A-Za-zА-Яа-я]/.test(name)) {
        return false;
    }

    if (/\d/.test(name)) {
        return false;
    }
    return true;
}


function validateBirthday(birthday) {
    let today = new Date();
    let birthDate = new Date(birthday);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age <= 100;
}

function createDeleteIcons() {
    let deleteIcons = document.querySelectorAll('.bi-x-square');
    deleteIcons.forEach(function (icon) {
        icon.removeEventListener('click', deleteStudent);
    });
    
    deleteIcons.forEach(function (icon) {
        icon.addEventListener('click', deleteStudent);
    });
}

function deleteStudent() {
    let icon = this;
    document.getElementById('delete-confirm-window').style.display = 'block';
    let confirmBtn = document.getElementById('confirm-delete-btn');
    let cancelBtn = document.getElementById('cancel-delete-btn');
    confirmBtn.addEventListener('click', function () {
        var row = icon.closest('tr');
        let groupCell = row.cells[1];
        let firstnameCell = row.cells[2];
        let nameCell = row.cells[3];
        let genderCell = row.cells[4];
        let birthdayCell = row.cells[5];

        console.log('Дані про студента, якого видалено:', {
            group: groupCell.textContent,
            firstname: firstnameCell.textContent,
            name: nameCell.textContent,
            gender: genderCell.textContent,
            birthday: birthdayCell.textContent
        });

        row.remove();
        document.getElementById('delete-confirm-window').style.display = 'none';
    });

    cancelBtn.addEventListener('click', function () {
        document.getElementById('delete-confirm-window').style.display = 'none';
    });
}


function setEditWindowValues(group, firstName, lastName, gender, birthday) {
    document.getElementById('editGroupSelect').value = group;
    document.getElementById('finame').value = firstName;
    document.getElementById('lname').value = lastName;
    document.getElementById('editGenderSelect').value = gender;
    document.getElementById('editBirthDate').value = birthday;
}

function addEditIconEventListeners() {
    let editIcons = document.querySelectorAll('.edit-icon');
    editIcons.forEach(function (editIcon) {
        editIcon.addEventListener('click', function () {
            let row = editIcon.closest('tr');
            let groupCell = row.cells[1];
            let firstnameCell = row.cells[2];
            let nameCell = row.cells[3];
            let genderCell = row.cells[4];
            let birthdayCell = row.cells[5];

            document.getElementById('editGroupSelect').value = groupCell.textContent;
            document.getElementById('finame').value = firstnameCell.textContent;
            document.getElementById('lname').value = nameCell.textContent;
            document.getElementById('editGenderSelect').value = genderCell.textContent;
            document.getElementById('editBirthDate').value = birthdayCell.textContent;

            document.getElementById('айді-вікна').style.display = "block";

            document.getElementById('save-edited-student-btn').onclick = function () {
                groupCell.textContent = document.getElementById('editGroupSelect').value;
                firstnameCell.textContent = document.getElementById('finame').value;
                nameCell.textContent = document.getElementById('lname').value;
                genderCell.textContent = document.getElementById('editGenderSelect').value;
                birthdayCell.textContent = document.getElementById('editBirthDate').value;
            
               
                let updatedData = {
                    group: groupCell.textContent,
                    firstname: firstnameCell.textContent,
                    name: nameCell.textContent,
                    gender: genderCell.textContent,
                    birthday: birthdayCell.textContent
                };
            
                console.log('Оновлені дані:', updatedData);
                document.getElementById('айді-вікна').style.display = "none";
            };
            
        });
    });
}
addEditIconEventListeners();
function getStudentData() {
    let group = document.getElementById('editGroupSelect').value;
    let firstname = document.getElementById('finame').value;
    let name = document.getElementById('lname').value;
    let gender = document.getElementById('editGenderSelect').value;
    let birthday = document.getElementById('editBirthDate').value;

    let studentData = "Group: " + group + ", First Name: " + firstname + ", Last Name: " + name + ", Gender: " + gender + ", Birthday: " + birthday;

    return studentData;
}

let bellIcon = document.getElementById('bell-icon');
let notificationWindow = document.getElementById('notification-window');

bellIcon.addEventListener('mouseenter', function () {
    notificationWindow.style.display = 'block';
});

bellIcon.addEventListener('mouseleave', function () {
    notificationWindow.style.display = 'none';
});


let profileIcon = document.getElementById('profile-icon');
let profileWindow = document.getElementById('profile-window');

profileIcon.addEventListener('mouseenter', function () {
    profileWindow.style.display = 'block';
});

profileIcon.addEventListener('mouseleave', function () {
    profileWindow.style.display = 'none';
});

profileWindow.addEventListener('mouseenter', function () {
    profileWindow.style.display = 'block';
});

profileWindow.addEventListener('mouseleave', function () {
    profileWindow.style.display = 'none';
});

function addSelectAllCheckboxEventListeners() {
    const selectAllCheckbox = document.querySelector('.select-all');
    selectAllCheckbox.addEventListener('change', function () {
        const rowCheckboxes = document.querySelectorAll('table input[type="checkbox"]');
        rowCheckboxes.forEach(function (cb) {
            cb.checked = selectAllCheckbox.checked;
        });
    });
}
