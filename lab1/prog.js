document.getElementById('add-student-btn').onclick = function () {
    document.getElementById('айді-вікна').style.display = "block";
}

document.getElementById('close-window-btn').onclick = function () {
    document.getElementById('айді-вікна').style.display = "none";
}

document.getElementById('close-window-btn').onclick = function () {
    let group = document.getElementById('gname').value;
    let name = document.getElementById('lname').value;
    let gender = document.getElementById('fname').value;
    let birthday = document.getElementById('age').value;
    let status = document.getElementById('sname').value;
    let rows = document.querySelectorAll('table ');
    let lastStudentRowIndex = -1;
    for (let i = 1; i < rows.length; i++) { 
        let cells = rows[i].getElementsByTagName('td');
        if (cells.length > 0) { 
            lastStudentRowIndex = i;
        } else { 
            break; 
        }
    }
    let table = document.querySelector('table');
    let newRow;
    if (lastStudentRowIndex !== -1) {
        newRow = table.insertRow(lastStudentRowIndex + 1); 
    } else {
        newRow = table.insertRow(-1); 
    }
  
    let checkboxCell = newRow.insertCell(0);
    checkboxCell.innerHTML = '<input type="checkbox" style="transform: scale(1.2);">';

    let groupCell = newRow.insertCell(1);
    groupCell.textContent = group;

    let nameCell = newRow.insertCell(2);
    nameCell.textContent = name;

    let genderCell = newRow.insertCell(3);
    genderCell.textContent = gender;

    let birthdayCell = newRow.insertCell(4);
    birthdayCell.textContent = birthday;

    let statusCell = newRow.insertCell(5);
    statusCell.innerHTML = '<div class="status-indicator active"></div>';

    let optionsCell = newRow.insertCell(6);
    optionsCell.innerHTML = '<div class="button-container"><button style="border: none; background-color: #fff;font-size: 23px;"><i class="bi bi-pencil-square"></i></button><button style="border: none; background-color: #fff;font-size: 23px; "><i class="bi bi-x-square"></i></button></div>';
    createDeleteIcons();
    document.getElementById('айді-вікна').style.display = "none";
}
function createDeleteIcons() {
    let deleteIcons = document.querySelectorAll('.bi-x-square');
        deleteIcons.forEach(function(icon) {
        icon.addEventListener('click', function() {
            document.getElementById('delete-confirm-window').style.display = 'block';
            let confirmBtn = document.getElementById('confirm-delete-btn');
            let cancelBtn = document.getElementById('cancel-delete-btn');
            confirmBtn.addEventListener('click', function() {
                var row = icon.closest('tr');
                row.remove();
                document.getElementById('delete-confirm-window').style.display = 'none';
            });

            cancelBtn.addEventListener('click', function() {
                document.getElementById('delete-confirm-window').style.display = 'none';
            });
        });
    });
}

let bellIcon = document.getElementById('bell-icon');
let notificationWindow = document.getElementById('notification-window');

bellIcon.addEventListener('mouseenter', function() {
    notificationWindow.style.display = 'block';
});

bellIcon.addEventListener('mouseleave', function() {
    notificationWindow.style.display = 'none';
});


let profileIcon = document.getElementById('profile-icon');
let profileWindow = document.getElementById('profile-window');

profileIcon.addEventListener('mouseenter', function() {
    profileWindow.style.display = 'block';
});

profileIcon.addEventListener('mouseleave', function() {
    profileWindow.style.display = 'none';
});

profileWindow.addEventListener('mouseenter', function() {
    profileWindow.style.display = 'block';
});

profileWindow.addEventListener('mouseleave', function() {
    profileWindow.style.display = 'none';
});

bi-x-square.addEventListener("click", function(){
    openDelPopup(button2);
  })




